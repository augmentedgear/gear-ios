//
//  JamViewController.swift
//  Postar
//
//  Created by Nathan Gitter on 3/14/18.
//  Copyright © 2018 Nathan Gitter. All rights reserved.
//

import UIKit

class JamViewController: UIViewController {
    
    @IBAction func viewTapped(_ sender: UITapGestureRecognizer) {
        dismiss(animated: true, completion: nil)
    }
    
}
