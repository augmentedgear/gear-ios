//
//  WaveformView.swift
//  Postar
//
//  Created by Nathan Gitter on 3/13/18.
//  Copyright © 2018 Nathan Gitter. All rights reserved.
//

import UIKit

class WaveformView: UIView {
    
    // MARK: - Pubic API
    
    /// A value between 0 and 1 to plot on the waveform.
    /// Updating this value re-draws the waveform.
    public var value: Double = 0 {
        didSet { update() }
    }
    
    /// The time between updates to the `value` property.
    /// This is used to smoothly animate the waveform.
    public var updateInterval: TimeInterval = 0.05
    
    /// An aditional value to multiply by the height of the waveforms.
    /// A value > 1 will make the waves higher, and a value < 1 will make them lower.
    /// Default value is 1.
    public var boostMultiplier: CGFloat = 1
    
    /// The number of points to graph on the waveform.
    public var numberOfPoints: Int = 10
    
    /// If the view should be paused and not move.
    /// Setting this to false will re-enable the motion.
    public var isPaused = true
    
    /// The fill color of the waveform. Default is black.
    public var color: UIColor = .black {
        didSet {
            waveformLayer.fillColor = color.cgColor
        }
    }
    
    // MARK: - UI Elements
    
    private lazy var waveformLayer: CAShapeLayer = {
        let layer = CAShapeLayer()
        layer.isGeometryFlipped = true
        layer.strokeColor = nil
        layer.lineWidth = 3
        layer.fillColor = color.cgColor
        return layer
    }()
    
    // MARK: - Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        sharedInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        sharedInit()
    }
    
    private func sharedInit() {
        backgroundColor = .clear
        self.layer.addSublayer(waveformLayer)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        waveformLayer.frame = bounds
    }
    
    // MARK: - Rendering and Animation
    
    private var previousPoints = [CGPoint]()
    
    private func update() {
        
        // create the points in the waveform
        var points = [CGPoint]()
        points.append(CGPoint(x: 0, y: 0))
        for i in 1 ..< numberOfPoints {
            let fraction = CGFloat(i) / CGFloat(numberOfPoints)
            let multiplier = CGFloat(arc4random_uniform(100)) / 100 * boostMultiplier
            let dampingMultiplier: CGFloat = (fraction < 0.3 || fraction > 0.7) ? 0.75 : 1
            points.append(CGPoint(x: fraction * bounds.width, y: multiplier * dampingMultiplier * CGFloat(value) * bounds.height))
        }
        points.append(CGPoint(x: bounds.width, y: 0))
        
        // handle paused state
        if isPaused { points = previousPoints }
        previousPoints = points
        
        // animate the waveform
        let newPath = createQuadPath(points: points)
        let animation = CABasicAnimation(keyPath: "path")
        animation.fromValue = waveformLayer.path
        animation.toValue = newPath.cgPath
        animation.duration = updateInterval
        waveformLayer.path = newPath.cgPath
        waveformLayer.add(animation, forKey: "wave")
        
    }
    
    // MARK: - Helper Functions
    
    private func createQuadPath(points: [CGPoint]) -> UIBezierPath {
        let path = UIBezierPath()
        if points.count < 2 { return path }
        let firstPoint = points[0]
        let secondPoint = points[1]
        let firstMidpoint = midpoint(first: firstPoint, second: secondPoint)
        path.move(to: firstPoint)
        path.addLine(to: firstMidpoint)
        for index in 1 ..< points.count-1 {
            let currentPoint = points[index]
            let nextPoint = points[index + 1]
            let midPoint = midpoint(first: currentPoint, second: nextPoint)
            path.addQuadCurve(to: midPoint, controlPoint: currentPoint)
        }
        guard let lastLocation = points.last else { return path }
        path.addLine(to: lastLocation)
        return path
    }
    
    private func midpoint(first: CGPoint, second: CGPoint) -> CGPoint {
        return CGPoint(x: (first.x + second.x) / 2, y: (first.y + second.y) / 2)
    }
    
}
