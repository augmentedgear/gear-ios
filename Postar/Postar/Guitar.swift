//
//  Guitar.swift
//  Postar
//
//  Created by Nathan Gitter on 3/13/18.
//  Copyright © 2018 Nathan Gitter. All rights reserved.
//

import CoreGraphics

enum PopupType {
    case tuning
    case neck
    case pickups
    case controls
    case body
}

extension PopupType {
    var string: String {
        switch self {
        case .tuning:
            return "Tuning"
        case .neck:
            return "Neck"
        case .pickups:
            return "Pickups"
        case .controls:
            return "Controls"
        case .body:
            return "Body"
        }
    }
}

extension PopupType {
    static var all: [PopupType] {
        return [.tuning, .neck, .pickups, .controls, .body]
    }
}

struct Guitar {
    
    // to display in music player
    var name: String
    var desc: String
    var songName: String
    
    // for links
    var productId: String
    
    // for recognition
    var imageName: String
    var width: CGFloat
    
    // for AR popup display
    var tuning: String
    var neck: String
    var pickups: String
    var controls: String
    var body: String
    
}

extension Guitar {
    
    static var allGuitars: [Guitar] {
        return [
            Guitar(name: "Danelectro DC59", desc: "The 6-String Danelectro DC 59 Electric Guitar captures the jangly sounds of the ’60s with a double-cutaway body made of laminated wood and 2 lipstick pickups.", songName: "guitar.mp3", productId: "danelectrodc59", imageName: "guitar", width: 0.335, tuning: "Gotoh", neck: "Maple, 25\", 21 frets", pickups: "Lipstick neck and bridge", controls: "Master Volume, Tone", body: "Double cutaway")
        ]
    }
    
}
