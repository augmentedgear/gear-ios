//
//  PlayPauseButton.swift
//  Postar
//
//  Created by Nathan Gitter on 3/13/18.
//  Copyright © 2018 Nathan Gitter. All rights reserved.
//

import UIKit

/// A button to play and pause media.
/// Inspired by the animated button in the Music app.
class PlayPauseButton: UIControl {
    
    // MARK: - Public API
    
    public var action: ((PlayPauseButton) -> ())?
    
    public var isPaused = false {
        didSet {
            playImageView.alpha = isPaused ? 0 : 1
            pauseImageView.alpha = isPaused ? 1 : 0
        }
    }
    
    // MARK: - UI Elements
    
    private lazy var circleView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(white: 0.9, alpha: 1)
        view.alpha = 0
        return view
    }()
    
    private lazy var playImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "play")
        return imageView
    }()
    
    private lazy var pauseImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "pause")
        imageView.alpha = 0
        return imageView
    }()
    
    // MARK: - Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        sharedInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        sharedInit()
    }
    
    private func sharedInit() {
        backgroundColor = .clear
        addTarget(self, action: #selector(touchUpInside), for: .touchUpInside)
        initialLayout()
    }
    
    private func initialLayout() {
        
        addSubview(circleView)
        circleView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        circleView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        circleView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        circleView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        
        addSubview(playImageView)
        playImageView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        playImageView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        playImageView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        playImageView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        
        addSubview(pauseImageView)
        pauseImageView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        pauseImageView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        pauseImageView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        pauseImageView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        circleView.layer.cornerRadius = circleView.bounds.width / 2
    }
    
    // MARK: - Actions
    
    @objc private func touchUpInside() {
        isPaused = !isPaused
        animate()
        action?(self)
    }
    
    // MARK: - Animation
    
    private func animate() {
        
        // swap images
        playImageView.alpha = isPaused ? 0 : 1
        pauseImageView.alpha = isPaused ? 1 : 0
        
        // bounce animation
        UIView.animate(withDuration: 0.15, delay: 0, options: [.curveEaseOut], animations: {
            self.playImageView.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
            self.pauseImageView.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        }, completion: { _ in
            UIView.animate(withDuration: 0.2, delay: 0, options: [.curveEaseIn], animations: {
                self.playImageView.transform = .identity
                self.pauseImageView.transform = .identity
            }, completion: nil)
        })
        
        // fade circle animation
        circleView.alpha = 1
        UIView.animate(withDuration: 0.3, delay: 0, options: [], animations: {
            self.circleView.alpha = 0
        }, completion: nil)
        
    }
    
}
