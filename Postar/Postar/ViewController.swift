//
//  ViewController.swift
//  Postar
//
//  Created by Nathan Gitter on 3/13/18.
//  Copyright © 2018 Nathan Gitter. All rights reserved.
//

import UIKit
import SceneKit
import ARKit
import AudioKit
import AudioKitUI

class ViewController: UIViewController, ARSCNViewDelegate, ARSessionDelegate {

    @IBOutlet var sceneView: ARSCNView!
    
    @IBOutlet weak var waveformView: WaveformView! {
        didSet {
            waveformView.updateInterval = 0.05
            waveformView.color = .white
            waveformView.boostMultiplier = 2
        }
    }
    
    @IBOutlet weak var label: UILabel!
    
    @IBOutlet weak var detailLabel: UILabel! {
        didSet {
            let attributedString = NSMutableAttributedString(string: detailLabel.text ?? "")
            let fullRange = NSRange(location: 0, length: attributedString.length)
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineHeightMultiple = 1.15
            paragraphStyle.alignment = .left
            attributedString.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraphStyle, range: fullRange)
            attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: detailLabel.textColor, range: fullRange)
            attributedString.addAttribute(NSAttributedStringKey.font, value: detailLabel.font, range: fullRange)
            detailLabel.attributedText = attributedString
        }
    }
    
    private var playerPosition: Double = 10
    
    @IBOutlet weak var playPauseButton: PlayPauseButton! {
        didSet {
            playPauseButton.action = { button in
                if button.isPaused {
                    self.player.play(from: self.playerPosition)
                    self.waveformView.isPaused = false
                } else {
                    self.playerPosition = self.player.currentTime
                    self.player.stop()
                    self.waveformView.isPaused = true
                }
            }
        }
    }
    
    @IBOutlet weak var playerBottomConstraint: NSLayoutConstraint!
        
    private var player = AKPlayer()
    
    private var mostRecentGuitar: Guitar?
    
    private var timer = Timer()
    
    /// A serial queue for thread safety when modifying the SceneKit node graph.
    let updateQueue = DispatchQueue(label: Bundle.main.bundleIdentifier! + ".serialSceneKitQueue")
    
    /// Convenience accessor for the session owned by ARSCNView.
    var session: ARSession {
        return sceneView.session
    }
    
    // MARK: - View Controller Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        sceneView.delegate = self
        sceneView.session.delegate = self
        
        navigationController?.navigationBar.isHidden = true
        playerBottomConstraint.constant = -400
        
    }
    
    private var hasViewAppeared = false
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if hasViewAppeared { return }
        hasViewAppeared = true
        
        // Prevent the screen from being dimmed to avoid interuppting the AR experience.
        UIApplication.shared.isIdleTimerDisabled = true
        
        // Start the AR experience
        resetTracking()
        
        // play some tunes
        let file = try! AKAudioFile(readFileName: "guitar.mp3")
        player = AKPlayer(audioFile: file)
        let tracker = AKAmplitudeTracker(player)
        AudioKit.output = tracker
        try! AudioKit.start()
        player.stop()
        
        // set up timer
        timer = Timer.scheduledTimer(withTimeInterval: 0.05, repeats: true) { timer in
            self.waveformView.value = tracker.amplitude
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
//        session.pause()
    }
    
//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        if isMusicPlayerShown {
//            hideMusicPlayer()
//        } else {
//            showMusicPlayer()
//        }
//    }
    
    private func showDetailViewController(urlString: String, title: String) {
        urlStringToPush = urlString
        titleToPush = title
        self.performSegue(withIdentifier: "detail", sender: self)
    }
    
    private var urlStringToPush: String = ""
    private var titleToPush: String = ""
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detail" {
            let detailViewController = segue.destination as? DetailViewController
            detailViewController?.urlString = urlStringToPush
            detailViewController?.title = titleToPush
        }
    }
    
    private var isMusicPlayerShown = false
    
    private func showMusicPlayer() {
        playerBottomConstraint.constant = 0
        isMusicPlayerShown = true
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 0, options: [], animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
        
        // start audio
        self.player.play(from: self.playerPosition)
        self.waveformView.isPaused = false
        playPauseButton.isPaused = true
    }
    
    private func hideMusicPlayer() {
        playerBottomConstraint.constant = -400
        isMusicPlayerShown = false
        UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    @IBAction func moreInfoButtonPressed(_ sender: UIButton) {
        let moreInfoUrlString = "/product/guitar/\(mostRecentGuitar?.productId ?? "")/details"
        showDetailViewController(urlString: moreInfoUrlString, title: mostRecentGuitar?.name ?? "")
    }
    
    @IBAction func callButtonPressed(_ sender: UIButton) {
        showDetailViewController(urlString: "/salesrep/guitars", title: "Sales Rep")
    }
    
    @IBAction func jamButtonPressed(_ sender: UIButton) {
        performSegue(withIdentifier: "jam", sender: self)
    }
    
    // MARK: - Session management (Image detection setup)
    
    /// Prevents restarting the session while a restart is in progress.
    var isRestartAvailable = true
    
    /// Creates a new AR configuration to run on the `session`.
    func resetTracking() {
        
        cardButtons.removeAll()
        hideMusicPlayer()
        
        var referenceImages = Set<ARReferenceImage>()
        
        let posterReferenceImage = ARReferenceImage(UIImage(named: "test")!.cgImage!, orientation: .up, physicalWidth: 0.256)
        posterReferenceImage.name = "test"
        referenceImages.insert(posterReferenceImage)
        
        for guitar in Guitar.allGuitars {
            let refImage = ARReferenceImage(UIImage(named: guitar.imageName)!.cgImage!, orientation: .up, physicalWidth: guitar.width)
            refImage.name = guitar.imageName
            referenceImages.insert(refImage)
        }
        
        let configuration = ARWorldTrackingConfiguration()
        configuration.detectionImages = referenceImages
        session.run(configuration, options: [.resetTracking, .removeExistingAnchors])
        
    }
    
    // these arrays are parallel, sorry
    private var cardButtons = [SCNNode]()
    private var popupTypes = [PopupType]()
    
    @IBAction func resetButtonPressed(_ sender: UIButton) {
        restartExperience()
    }
    
    @IBAction func viewTapped(_ sender: UITapGestureRecognizer) {
        let cardHitTestResults = sceneView.hitTest(sender.location(in: sceneView), options: nil)
        for result in cardHitTestResults {
            for (button, type) in zip(cardButtons, popupTypes) {
                if button == result.node {
                    let guitar = Guitar.allGuitars.filter { $0.imageName == result.node.name }.first!
                    var urlString = "/product/guitar/\(guitar.productId)/"
                    switch type {
                    case .tuning:
                        urlString += "tuning"
                    case .neck:
                        urlString += "neck"
                    case .pickups:
                        urlString += "pickups"
                    case .controls:
                        urlString += "controls"
                    case .body:
                        urlString += "body"
                    }
                    showDetailViewController(urlString: urlString, title: guitar.name)
                }
            }
        }
    }
    
    // MARK: - ARSCNViewDelegate (Image detection results)
    
    /// - Tag: ARImageAnchor-Visualizing
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        
        guard let imageAnchor = anchor as? ARImageAnchor else { return }
        let referenceImage = imageAnchor.referenceImage
        
        updateQueue.async {
            
//            // Create a plane to visualize the initial position of the detected image.
//            let plane = SCNPlane(width: referenceImage.physicalSize.width,
//                                 height: referenceImage.physicalSize.height)
//            let planeNode = SCNNode(geometry: plane)
//            planeNode.opacity = 0.1
//            planeNode.eulerAngles.x = -.pi / 2
//            planeNode.runAction(self.imageHighlightAction)
//            node.addChildNode(planeNode)
            
        }
        
        DispatchQueue.main.async {
            
            let imageName = referenceImage.name ?? ""
            let guitar = Guitar.allGuitars.filter { $0.imageName == imageName }.first!
            
            // Create AR popup views
            for (index, type) in PopupType.all.enumerated() {
                let point: CGPoint
                switch type {
                case .tuning:
                    point = CGPoint(x: 0.1, y: 0.4)
                case .neck:
                    point = CGPoint(x: 0.10, y: 0.15)
                case .pickups:
                    point = CGPoint(x: 0, y: -0.3)
                case .controls:
                    point = CGPoint(x: 0.2, y: -0.4)
                case .body:
                    point = CGPoint(x: -0.18, y: -0.35)
                }
                self.addPopupNode(toNode: node, atRelativePosition: point, guitar: guitar, type: type, delayIndex: index)
            }
            
            // get the guitar object of choice
            self.label.text = guitar.name
            self.detailLabel.text = guitar.desc
            self.mostRecentGuitar = guitar
            
            // reset all the audio stuff
            let file = try! AKAudioFile(readFileName: guitar.songName)
            self.player = AKPlayer(audioFile: file)
            let tracker = AKAmplitudeTracker(self.player)
            AudioKit.output = tracker
            try! AudioKit.start()
            self.player.stop()
            
            // reset timer
            self.timer.invalidate()
            self.timer = Timer.scheduledTimer(withTimeInterval: 0.05, repeats: true) { timer in
                self.waveformView.value = tracker.amplitude
            }
            
            // show music player popup
            self.showMusicPlayer()
            
        }
        
    }
    
    private func addPopupNode(toNode node: SCNNode, atRelativePosition relativePosition: CGPoint, guitar: Guitar, type: PopupType, delayIndex: Int) {
        
        let billboardConstraint = SCNBillboardConstraint()
        billboardConstraint.freeAxes = SCNBillboardAxis.Y
        
        let textNode = SCNNode()
        textNode.scale = SCNVector3(x: 0.1, y: 0.1, z: 0.1)
        node.addChildNode(textNode)
        textNode.position.x += Float(relativePosition.x)
        textNode.position.z += Float(-relativePosition.y)
        let backNode = SCNNode()
        let plaque = SCNBox(width: 0.14, height: 0.1, length: 0.01, chamferRadius: 0.005)
        plaque.firstMaterial?.diffuse.contents = UIColor(white: 1, alpha: 0.75)
        backNode.geometry = plaque
        backNode.position.y += 0.09
        
        let imageView = UIView(frame: CGRect(x: 0, y: 0, width: 800, height: 700))
        imageView.backgroundColor = .clear
        imageView.alpha = 1.0
        let titleLabel = UILabel(frame: CGRect(x: 64, y: 45, width: imageView.frame.width - 224, height: 130))
        titleLabel.font = UIFont.systemFont(ofSize: 140, weight: UIFont.Weight.bold)
        titleLabel.textAlignment = .left
        titleLabel.numberOfLines = 1
        titleLabel.text = type.string
        titleLabel.backgroundColor = .clear
        imageView.addSubview(titleLabel)
//        let subLabel = UILabel(frame: CGRect(x: 64, y: 140, width: imageView.frame.width - 224, height: 200))
//        subLabel.font = UIFont.systemFont(ofSize: 80, weight: UIFont.Weight.medium)
//        subLabel.textAlignment = .left
//        subLabel.numberOfLines = 2
//        subLabel.text = {
//            switch type {
//            case .tuning:
//                return guitar.tuning
//            case .neck:
//                return guitar.neck
//            case .pickups:
//                return guitar.pickups
//            case .controls:
//                return guitar.controls
//            case .body:
//                return guitar.body
//            }
//        }()
//        subLabel.backgroundColor = .clear
//        imageView.addSubview(subLabel)
        
        let buttonNode = self.createButton(size: CGSize(width: imageView.frame.width - 128, height: 230))
        buttonNode.name = guitar.imageName
        self.cardButtons.append(buttonNode)
        self.popupTypes.append(type)
        
        let texture = UIImage.imageWithView(view: imageView)
        
        let infoNode = SCNNode()
        let infoGeometry = SCNPlane(width: 0.13, height: 0.09)
        infoGeometry.firstMaterial?.diffuse.contents = texture
        infoNode.geometry = infoGeometry
        infoNode.position.y += 0.09
        infoNode.position.z += 0.0055
        
        textNode.addChildNode(backNode)
        textNode.addChildNode(infoNode)
        
        infoNode.addChildNode(buttonNode)
        buttonNode.position = infoNode.position
        buttonNode.position.y -= 0.105
        
        textNode.constraints = [billboardConstraint]
        
        // aniation
        textNode.runAction(SCNAction.scale(to: 0.0, duration: 0))
        backNode.runAction(SCNAction.scale(to: 0.0, duration: 0))
        infoNode.runAction(SCNAction.scale(to: 0.0, duration: 0))
        textNode.runAction(SCNAction.fadeOpacity(to: 0.0, duration: 0))
        backNode.runAction(SCNAction.fadeOpacity(to: 0.0, duration: 0))
        infoNode.runAction(SCNAction.fadeOpacity(to: 0.0, duration: 0))

        let waitDuration: TimeInterval = 1.5
        let textWait = SCNAction.wait(duration: waitDuration + 0.2 * delayIndex)
        let backWait = SCNAction.wait(duration: waitDuration + 0.2 * delayIndex)
        let infoWait = SCNAction.wait(duration: waitDuration + 0.2 * delayIndex)
        
        let duration: TimeInterval = 0.6
        let textScale = SCNAction.scale(to: 1.0, duration: duration)
        let backScale = SCNAction.scale(to: 1.0, duration: duration)
        let infoScale = SCNAction.scale(to: 1.0, duration: duration)
        let textFade = SCNAction.fadeOpacity(to: 1.0, duration: duration)
        let backFade = SCNAction.fadeOpacity(to: 1.0, duration: duration)
        let infoFade = SCNAction.fadeOpacity(to: 1.0, duration: duration)
        
        textNode.runAction(SCNAction.sequence([textWait, SCNAction.group([textScale, textFade])]))
        backNode.runAction(SCNAction.sequence([backWait, SCNAction.group([backScale, backFade])]))
        infoNode.runAction(SCNAction.sequence([infoWait, SCNAction.group([infoScale, infoFade])]))
        
    }
    
    private func createButton(size: CGSize) -> SCNNode {
        let buttonNode = SCNNode()
        let buttonView = UIView(frame: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        buttonView.backgroundColor = blueColor
        let buttonLabel = UILabel(frame: CGRect(x: 0, y: 0, width: buttonView.frame.width, height: buttonView.frame.height))
        buttonLabel.backgroundColor = .clear
        buttonLabel.textColor = .white
        buttonLabel.text = "Info"
        buttonLabel.font = UIFont.systemFont(ofSize: 75, weight: UIFont.Weight.bold)
        buttonLabel.textAlignment = .center
        buttonView.addSubview(buttonLabel)
        buttonNode.geometry = SCNBox(width: size.width / 6000, height: size.height / 6000, length: 0.002, chamferRadius: 0.0)
        let textMaterial = SCNMaterial()
        textMaterial.diffuse.contents = UIImage.imageWithView(view: buttonView)
        let blueMaterial = SCNMaterial()
        blueMaterial.diffuse.contents = blueColor
        buttonNode.geometry?.materials = [textMaterial, blueMaterial, blueMaterial, blueMaterial, blueMaterial, blueMaterial]
        return buttonNode
    }
    
    var imageHighlightAction: SCNAction {
        return .sequence([
            .wait(duration: 0.25),
            .fadeOpacity(to: 0.85, duration: 0.25),
            .fadeOpacity(to: 0.15, duration: 0.25),
            .fadeOpacity(to: 0.85, duration: 0.25),
            //            .fadeOut(duration: 0.5),
            //            .removeFromParentNode()
            ])
    }
    
    // MARK: - ARSessionDelegate
    
    func session(_ session: ARSession, cameraDidChangeTrackingState camera: ARCamera) {
//        switch camera.trackingState {
//        case .notAvailable, .limited:
////            statusViewController.escalateFeedback(for: camera.trackingState, inSeconds: 3.0)
//        case .normal:
////            statusViewController.cancelScheduledMessage(for: .trackingStateEscalation)
//        }
    }
    
    func session(_ session: ARSession, didFailWithError error: Error) {
        guard error is ARError else { return }
        
        let errorWithInfo = error as NSError
        let messages = [
            errorWithInfo.localizedDescription,
            errorWithInfo.localizedFailureReason,
            errorWithInfo.localizedRecoverySuggestion
        ]
        
        // Use `flatMap(_:)` to remove optional error messages.
        let errorMessage = messages.compactMap({ $0 }).joined(separator: "\n")
        
        DispatchQueue.main.async {
            self.displayErrorMessage(title: "The AR session failed.", message: errorMessage)
        }
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        //
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        restartExperience()
    }
    
    func sessionShouldAttemptRelocalization(_ session: ARSession) -> Bool {
        return true
    }
    
    // MARK: - Error handling
    
    func displayErrorMessage(title: String, message: String) {
        // Present an alert informing about the error that has occurred.
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let restartAction = UIAlertAction(title: "Restart Session", style: .default) { _ in
            alertController.dismiss(animated: true, completion: nil)
            self.resetTracking()
        }
        alertController.addAction(restartAction)
        present(alertController, animated: true, completion: nil)
    }
    
    // MARK: - Interface Actions
    
    func restartExperience() {
        
        guard isRestartAvailable else { return }
        isRestartAvailable = false
                
        resetTracking()
        
        // Disable restart for a while in order to give the session time to restart.
        DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
            self.isRestartAvailable = true
        }
        
    }
    
}
