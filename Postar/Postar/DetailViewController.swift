//
//  DetailViewController.swift
//  Postar
//
//  Created by Nathan Gitter on 3/13/18.
//  Copyright © 2018 Nathan Gitter. All rights reserved.
//

import UIKit
import WebKit

class DetailViewController: UIViewController {
    
    /// The url to load in the view controller.
    public var urlString: String?
    
    private let baseUrlString = "https://demo.gearhunt.io"
    
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        webView.navigationDelegate = self
        
        var fullUrlString = baseUrlString
        fullUrlString += urlString ?? ""
        print("loading: \(fullUrlString)")
        webView.load(URLRequest(url: URL(string: fullUrlString)!))
        
        activityIndicator.hidesWhenStopped = true
        activityIndicator.startAnimating()
        
    }
    
}

extension DetailViewController: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        activityIndicator.stopAnimating()
    }
    
}
