//
//  UIKitExtensions.swift
//  Postar
//
//  Created by Nathan Gitter on 3/13/18.
//  Copyright © 2018 Nathan Gitter. All rights reserved.
//

import UIKit

extension UIImage {
    class func imageWithView(view: UIView) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, false, 0.0)
        view.drawHierarchy(in: view.bounds, afterScreenUpdates: true)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img!
    }
}
